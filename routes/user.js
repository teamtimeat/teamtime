function checkUserExists(username) {
    var stmt = "select user_name from `users` where user_name = '" + username + "'";
    var query = db.query(stmt, function (err, result) {
        if (result.length > 0) {
            console.log("Users found");
            return true;
        }
        console.log("No Users found");

        return false;
    })
}

//---------------------------------------------signup page call------------------------------------------------------
exports.signup = function (req, res) {
    message = '';
    if (req.method == "POST") {
        var post = req.body;
        var name = post.user_name;
        var pass = post.password;
        var fname = post.first_name;
        var lname = post.last_name;
        var mob = post.mob_no;


        var stmt = "select user_name from `users` where user_name = '" + name + "'";
        var query = db.query(stmt, function (err, result) {
            if (result.length > 0) {
                res.render('signup', {message: "User allready exists"});
            } else {
                var sql = "INSERT INTO `users`(`first_name`,`last_name`,`mob_no`,`user_name`, `password`) VALUES ('" + fname + "','" + lname + "','" + mob + "','" + name + "','" + pass + "')";

                var query = db.query(sql, function (err, result) {
                    console.log(query.result);
                    message = "Succesfully! Your account has been created.";
                    res.render('index.ejs', {message: message});
                });
                db.commit();
            }
        });
    }
    else {
        res.render('signup');
    }
}
;
exports.projectDetails = function (req, res) {
    console.log("Test");
    if (req.method == "POST") {
        var post = req.body;
        var desc = post.project_description;
        var name = post.project_name;
        var start = post.project_start;
        var end = post.project_end;

        var ins = "insert into event (name, description" +
            ", start_date, end_date, location) values( " +
            "'" + name + "', " + "'" + desc + "', str_to_date('" + start + "', '%Y-%m-%d') "+ ", str_to_date('" + end + "', '%Y-%m-%d')" + ",'location')"

        console.log(ins);
        db.query(ins, function (err, results) {

        });
        db.commit();

        var sql = "select id, concat(first_name ,' ' , last_name) as name from person";
        var team_lead;
        var event;
        db.query(sql, function (err, results) {
            if (results.length) {
                team_lead = results;
                console.log(results);

            }

        });

        var get_events = "SELECT id, name FROM event";
        db.query(get_events, function (err, results) {
            if (results.length) {
                event = results;
                console.log(results);
            }

        });
        console.log(event);
        //res.render('home/new_project.ejs');
        res.render('team_lead.ejs', {team_leader: team_lead, event_val: event});

    }

}
;

//-----------------------------------------------login page call------------------------------------------------------
exports.login = function (req, res) {
    var message = '';
    var sess = req.session;

    if (req.method == "POST") {
        var post = req.body;
        var name = post.user_name;
        var pass = post.password;


        var sql = "SELECT id, first_name, last_name, user_name FROM `users` WHERE `user_name`='" + name + "' and password = '" + pass + "'";
        db.query(sql, function (err, results) {
            if (results.length) {
                req.session.userId = results[0].id;
                req.session.user = results[0];
                console.log(results[0].id);
                res.redirect('/home/dashboard');
            }
            else {
                message = 'Wrong Credentials.';
                res.render('index.ejs', {message: message});
            }

        });
    } else {
        res.render('index.ejs', {message: message});
    }

};

//-----------------------------------------------login page call------------------------------------------------------
exports.createProject = function (req, res) {
    var message = '';
    var sess = req.session;

    res.render('new_project.ejs');


};
//-----------------------------------------------dashboard page functionality----------------------------------------------

exports.dashboard = function (req, res, next) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('ddd=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    db.query(sql, function (err, results) {
        res.render('dashboard.ejs', {user: user});
    });
};
//------------------------------------logout functionality----------------------------------------------
exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.redirect("/login");
    })
};
//--------------------------------render user details after login--------------------------------
exports.profile = function (req, res) {

    var userId = req.session.userId;
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";
    db.query(sql, function (err, result) {
        res.render('profile.ejs', {data: result});
    });
};
//---------------------------------edit users details after login----------------------------------
exports.editprofile = function (req, res) {
    var userId = req.session.userId;
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";
    db.query(sql, function (err, results) {
        res.render('edit_profile.ejs', {data: results});
    });
};
